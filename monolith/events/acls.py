import requests
from .keys import PEXELS_API_KEY


def get_photos(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = response.json()
    return {"picture_url": content["photos"][0]["src"]["original"]}
